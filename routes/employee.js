var  express=require("express");
var  router=express.Router();
var  shared=require('./shared');

/** This URL is used for to register the data to mongodb */
router.post('/emp-register',function(req,res){
    var fname=req.body.firstname;
    var lname=req.body.lastname;
    var email=req.body.email;
    var pwd=req.body.password;

    var obj={
        firstname:fname,
        lastname:lname,
        email:email,
        password:pwd
    }
    shared.getMongodbConnection(res,function(db){
        db.collection('Employee').insertOne(obj,function(err,result){
            if(err){
                res.send("Mongodb registration failed");
            }
            else{
                res.send(result);
            }
        })
    })
})


/** This url is used for to compare the data coming from client to server */
router.post('/login',function(req,res){
    shared.getMongodbConnection(res,function(db){
       db.collection('Employee').find({
            "email":req.body.email,
            "password":req.body.password
        }).toArray(function (err,user){
           if(err){
               res.send(err);
           }
            else{
                if(user.length>0)
                {
                    let  token=require("./generateToken")({
                        email:req.body.email,
                        password:req.body.password
                    },"appalaswamy@tcs.com");
                    res.send({"login":"success","token":token,user});
                }
                else{
                    res.send({"login":"fail"});
                }
            }
       });
    })
})



/** This data is used for to get the data from mongodb */
router.get("/getEmp",function(req,res){
    shared.getMongodbConnection(res,function(db){
        db.collection('Employee').find().toArray(function(err,Array){
            if(err){
                res.send("Mongodb Fetch query error");
            }
            else{
                res.send(Array);
            }
        })
    })
})
module.exports=router;