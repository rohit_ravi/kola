let mongodb=require('mongodb');
let sharedObj={};
sharedObj.getMongodbConnection=function(res,cb){
    let mongoClient=mongodb.MongoClient;
    let url="mongodb://localhost:27017";
    mongoClient.connect(url,function(err,cluster){
        if(err){
            res.send("Mongodb connections error");
        }
        var db=cluster.db('Register');
        cb(db);
    })
}
module.exports=sharedObj;